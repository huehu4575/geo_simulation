function Ak = calc_Ak(omega_k)
    global Ts
    if isrow(omega_k)
        omega_k=omega_k.';
    end
    no = norm(omega_k);
    wk = no*Ts*0.5;
    Ak=zeros(4,4);
    if no>0
        gk=sin(wk)*(1/no)*omega_k;
    else
        gk=[0;0;0];
    end
    cwk = cos(wk);
    hgk = omega_to_omegahat(gk);
    
    Ak(1,1)   = cwk;
    Ak(2:4,1) = gk;
    Ak(1,2:4) = (-1) * gk.';
    Ak(2:4, 2:4) = cwk*eye(3)-hgk;
end