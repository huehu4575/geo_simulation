function [q_kp, omega_kp] = ContSystem_one_step(q_k, omega_k, u_k)
    global Ts
    x_0=[q_k; omega_k];
    [T,x_kp] = ode45(@(t,x)ContSystem(x,u_k),[0 Ts],x_0);
    q_kp     = x_kp(end, 1:4);
    omega_kp = x_kp(end, 5:7);
end


function [dx] = ContSystem(x,u)
    q=x(1:4);
    omega_h=[0; x(5:7)];
    
    dq = 0.5 * q_product(q,omega_h);
    domega = u;
    dx = [dq; domega];
end