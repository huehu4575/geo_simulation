% Numerical Integration using ODE45

function [xk_1, uk_1] = pendulumDT0(xk, uk, wk, Ts)

g = cay(se3_hat(xk));

v = convVCaytoR(uk(1),uk(2),uk(3),uk(4),uk(5),uk(6));
a = convWCaytoR(wk(1),wk(2),wk(3),wk(4),wk(5),wk(6),uk(1),uk(2),uk(3),uk(4),uk(5),uk(6));

q = [g(:); v];

[T,X] = ode45(@(t,q1)pendulumCT0(q1,a),[0 Ts],q);
gk_1 = reshape(X(end,1:16),4,4);
vk_1 = X(end,17:22)';
xi=vk_1(1:3); eta=vk_1(4:6);
xic = (eta*eta'/2/norm(eta)^2 - so3_hat(eta)^2/norm(eta)^2*tan(norm(eta)/2)/norm(eta))*xi;
etac = tan(norm(eta)/2)/norm(eta)*eta;

xk_1 = se3_hatinv(cayInv(gk_1));
uk_1 = [xic;etac];
end

function dx = pendulumCT0(x,u)
g = reshape(x(1:16), 4, 4);
v = x(17:22);

dg = g*se3_hat(v);
dv = u;

dx = [dg(:); dv];
end
