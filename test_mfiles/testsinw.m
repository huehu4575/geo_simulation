clear all
close all
T=5;
om=[-1; 0; 0];
d=[0.001; 0; 0];
t=-1:0.001:1;
[a,b]=size(t);
g_his=[]
for i=1:b
   omega=om+d*i;
   no=norm(omega);
   g=[0;0;0];
   if no>0
      g=omega/no*sin(no*T/2);
   end
   g_his=[g_his g];
end
plot(t, g_his)