function q_out = q_cproduct(q1, q2)
    q1.z=q1(1);
    q1.v=q1(2:4);
    q2.z=q2(1);
    q2.v=q2(2:4);
    q_out.z=0;
    q_out.v=q1.z*q2.v+q2.z*q1.v+cross(q1.v, q2.v);
end