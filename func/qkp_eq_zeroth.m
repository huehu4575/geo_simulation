%  zero-th hold equation
function q_kp = qkp_eq_zeroth(q_k,omega_k,~)
    % q_{k+1} = q_k (otimes) q_omega
    if isrow(omega_k)
        omega_k=omega_k.';
    end
    qo=omega_to_qomega(omega_k);
    q_kp=q_product(q_k, qo);
end