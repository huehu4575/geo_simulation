function q_kp = qkp_eq_first(q_k,omega_k,omega_kp)
 % q_{k+1} = q_k (x) ( q_{omega_mid} +  Ts*Ts/24*[0; om_k X om_{k+1}] )
    global Ts 
    if isrow(omega_k)
        omega_k=omega_k.';
    end
    if isrow(omega_kp)
        omega_kp=omega_kp.';
    end
    omega_mid=(omega_k+omega_kp)/2;
    qo_mid=omega_to_qomega(omega_mid);
    om_first=cross(omega_k, omega_kp);
    if isrow(om_first)
        om_first=om_first.';
    end
    q_first=[0; om_first];
    q2 = qo_mid+Ts*Ts/24*q_first;
    q_kp=q_product(q_k, q2);
end