function rowq = qzqv_to_rowq(qz, qv)
    if isrow(qv)
        qv=qv.';
    end
    rowq=[qz; qv];
end