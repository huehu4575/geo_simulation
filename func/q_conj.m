function q_out = q_conj(q1)
    q_out=[q1(1); -q1(2:4)];
end