function [f, g] = calc_J(q_k, omega_k, bar_u)
    global Q4 Qo R p Terminalcost hold_order
    
    dim_barR = size(bar_u,1);        % size of input row (N=sum{dim(u_1)+dim(u_2)+...dim(u_N)})
    %predict_step_num=p;             %predict_step_num=dim(barR)/dim(u_*)
    barQ4 = zeros((dim_barR/3)*4); %mbr^{4p*4p}
    barQo = zeros(dim_barR);      %mbr^{3p*3p}
    barR = zeros(dim_barR);        %mbr^{3p*3p}
    for i = 1:p
        barQ4(i*4-3:i*4,i*4-3:i*4)=Q4; % barQ4=diag([Q4, Q4, ..., Q4])
        barQo(i*3-2:i*3,i*3-2:i*3)=Qo; % barQo=diag([Qo, Qo, ..., Qo])
         barR(i*3-2:i*3,i*3-2:i*3)=R;  % barR =diag([ R,  R, ..., R])
    end
    % add terminal cost
    barQ4(dim_barR-2:dim_barR,dim_barR-2:dim_barR) = Terminalcost*barQ4(dim_barR-2:dim_barR,dim_barR-2:dim_barR);
    
    %% calc {q, omega}_{k+1, ..., k+p}  from q_k, omega_k, u_{k, ..., k+p-1}
    [bar_q, bar_omega] = calc_state_p_step(q_k, omega_k, bar_u, hold_order);
    
    %% cost function
    breve_q=bar_q;
    for i=1:p
       breve_q(4*i-3)=bar_q(4*i-3)-1;
    end
    f = breve_q'*barQ4*breve_q/2+bar_omega'*barQo*bar_omega/2+bar_u'*barR*bar_u/2;
    
    %% grad J
    g = calc_gradJ(breve_q, bar_q, bar_omega, bar_u, barQ4, barQo, barR);
end
