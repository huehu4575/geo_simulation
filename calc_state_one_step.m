%No more used
function [q_kp, omega_kp] = calc_state_one_step(q_k, omega_k, u_k, hold_order)
    global Ts
    % LET: bar_*=zeros(size,1) **define as ROW vectors
    q_kp=zeros(4,1);  % q_k+1
    omega_kp=zeros(3,1); % omega_k+1

    % Calculate *_{k+1} from *_k
    omega_kp(1:3) = omega_k+u_k(1:3)*Ts;         % omega_{k+1} = omega_k + u_k*Ts
    q_kp(1:4)  = qkp_eq_first(q_k,omega_k,omega_kp);   % q_{k+1} = q_k (otimes) q_{omega_k}
        bar_omega(1:3) = omega_k+bar_u(1:3)*Ts;        % omega_{k+1} = omega_k + u_k*Ts
    if hold_order == 0
        q_kp(1:4)  = qkp_eq_zeroth(q_k,omega_k, bar_omega(1:3));   % q_{k+1} = q_k (otimes) q_{omega}
    elseif hold_order == 1
        q_kp(1:4)  = qkp_eq_first(q_k,omega_k, bar_omega(1:3));   % q_{k+1} = q_k (otimes) ( q_{omega_mid} +  Ts*Ts/24*[0; om_k X om_{k+1}] )
    end
end