function total_J = calc_total_J(N, q_his, om_his, u_his)
    global Q4 Qo R Terminalcost
    total_J=zeros(1,N+1);
    for k=1:N
        q_k     = q_his(1:4,k+1);
        omega_k = om_his(1:3,k+1);
        u_km    = u_his(1:3, k);
        qkqi    = q_k - [1;0;0;0];
        if k==N
            Q4=Terminalcost*Q4;
        end
        Jk=0.5*( qkqi'*Q4*qkqi + omega_k'*Qo*omega_k + u_km'*R*u_km );
        total_J(k+1)=total_J(k)+Jk;
    end
end